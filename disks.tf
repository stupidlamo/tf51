resource "google_compute_disk" "default8" {
  name  = "disk8"
  type  = "pd-ssd"
  size  = 8
  zone  = "us-central1-a"
}
resource "google_compute_disk" "default10" {
  name  = "disk10"
  type  = "pd-ssd"
  size  = 10
  zone  = "us-central1-a"
}
