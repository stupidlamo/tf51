output "hostname" {
  value = "${google_compute_instance.vm_instance.*.hostname}"
}
output "projectid" {
  value = "${google_compute_instance.vm_instance.*.project}"
}
