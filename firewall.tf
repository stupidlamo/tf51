variable "portnames" {
  type = list(string)
  default = ["80", "8001", "8802"]
}

resource "google_compute_firewall" "default" {
  name    = "tf52-tasks-tcp"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["80", "443"]
  }
}

resource "google_compute_firewall" "default2" {
  name  = "tf52-tasks-udp"
  network = "default"

  allow {
    protocol = "udp"
    ports    = ["10000-20000"]
  }

  destination_ranges = ["10.0.0.23/32"]
  direction  = "EGRESS"
}



resource "google_compute_firewall" "dynamic_default3" {
  name  = "tf53-tcp"
  network = "default"
  dynamic "allow" {
    for_each = var.portnames
    content {
      protocol = "tcp"
      ports    = [ allow.value ]
    }
  }
}
