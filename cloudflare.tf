variable "zone" {}

resource "cloudflare_record" "junewaysubdomain" {
  name = "stupidlamo.juneway.pro"
  value = "${google_compute_address.ip_address.address}"
  type = "A"
  ttl = 3600
  zone_id = var.zone
}

