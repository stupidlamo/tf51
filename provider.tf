variable "cloudflare_email" {}
variable "cloudflare_api" {}
variable "cloudflare_account" {}

terraform {
  required_providers {
      cloudflare = {
        source = "cloudflare/cloudflare"
      }
  }
}

provider "google" {
  project = "local-turbine-298316"
  region  = "us-central1"
  zone  = "us-central1-a"
}

provider "cloudflare" {
  version = "~> 2.0"
  email = var.cloudflare_email
  api_key = var.cloudflare_api
  account_id = var.cloudflare_account
}
