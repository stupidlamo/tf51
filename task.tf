variable "private_key_path" {
  description = "local usr key"
}
variable "nodec" {
  default = "2"
}

resource "google_compute_instance" "vm_instance" {
  count = "${var.nodec}"
  name  = "my-${count.index}-node"
  machine_type = "e2-small"
  hostname   = "my${count.index}node.juneway.pro"
  connection {
    user = "komroman"
    private_key = "${file(var.private_key_path)}"
    timeout = "2m"
    host = "${self.network_interface.0.network_ip}"
  }
  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
      size = 20
    }
  }
  network_interface {
  network = "default"
  }

  provisioner "local-exec" {
   command = "echo ${self.hostname}  ${self.network_interface.0.network_ip} >> host.list"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get install -y apt-transport-https  ca-certificates curl gnupg lsb-release",
      "sudo curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg",
      "sudo echo deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian stretch stable | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null",
      "sudo apt-get update && sudo apt-get install -y docker-ce docker-ce-cli containerd.io",
      "sudo docker run -p 80:80 --name debnginx -d nginx",
      "sudo docker exec debnginx rm /usr/share/nginx/html/index.html && sudo docker exec debnginx bash -c 'echo Juneway ${self.hostname} ${self.network_interface.0.network_ip} >> /usr/share/nginx/html/index.html'",
    ]
  }
}
resource "google_compute_forwarding_rule" "default" {
  name       = "website-forwarding-rule-54"
  target     = google_compute_target_pool.default.id
  port_range = "80"
}

resource "google_compute_target_pool" "default" {
  name = "website-target-pool-54"
  instances = [
    "us-central1-a/my-0-node",
    "us-central1-a/my-1-node",
  ]
}
